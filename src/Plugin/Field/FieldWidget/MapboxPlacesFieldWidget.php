<?php

namespace Drupal\mapbox_places\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'mapbox_places_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "mapbox_places_field_widget",
 *   label = @Translation("Mapbox Places"),
 *   field_types = {
 *     "text",
 *     "string"
 *   }
 * )
 */
class MapboxPlacesFieldWidget extends WidgetBase implements WidgetInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'size' => 60,
        'autocomplete' => TRUE,
        'types' => [
          'address',
          'country',
          'region',
          'place',
          'postcode',
          'locality',
          'neighborhood',
        ],
        'limit' => 5,
        'country' => '',
        'language' => 'en',
        'placeholder' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['autocomplete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autocomplete'),
      '#default_value' => $this->getSetting('autocomplete'),
      '#description' => $this->t('Specify whether to return autocomplete results (true, default) or not (false).'),
    ];

    $dataTypes = [
      'country' => $this->t('Country'),
      'region' => $this->t('Region'),
      'postcode' => $this->t('Postal Code'),
      'district' => $this->t('District'),
      'place' => $this->t('Place'),
      'locality' => $this->t('Locality'),
      'neighborhood' => $this->t('Neighborhood'),
      'address' => $this->t('Address'),
      'poi' => $this->t('Points of interest'),
    ];
    $elements['types'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $dataTypes,
      '#title' => $this->t('Data types'),
      '#default_value' => $this->getSetting('types'),
      '#description' => $this->t('The data types available in the geocoder, listed from the largest to the most granular.'),
    ];

    $elements['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Results limit'),
      '#default_value' => $this->getSetting('limit'),
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 10,
      '#description' => $this->t('Specify the maximum number of results to return. The default is 5 and the maximum supported is 10.'),
    ];

    $elements['country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Country'),
      '#default_value' => $this->getSetting('country'),
      '#description' => $this->t('Limit results to one or more countries. Permitted values are ISO 3166 alpha 2 country codes separated by commas.'),
    ];

    $elements['language'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Language'),
      '#default_value' => $this->getSetting('language'),
      '#description' => $this->t('Specify the user’s language. This parameter controls the language of the text supplied in responses, and also affects result scoring, with results matching the user’s query in the requested language being preferred over results that match in another language.'),
    ];

    $elements['size'] = [
      '#type' => 'number',
      '#title' => $this->t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];

    $elements['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    // Auto-complete.
    if (!empty($this->getSetting('autocomplete'))) {
      $summary[] = $this->t('Autocomplete: @autocomplete', ['@autocomplete' => ($this->getSetting('autocomplete')) ? $this->t('Yes') : $this->t('No')]);
    }

    // Types.
    if (!empty($this->getSetting('types'))) {
      $summary[] = $this->t('Types: @types', ['@types' => implode(', ', $this->getSetting('types'))]);
    }

    // Limit.
    if (!empty($this->getSetting('limit'))) {
      $summary[] = $this->t('Results limit: @limit', ['@limit' => $this->getSetting('limit')]);
    }

    // Country.
    if (!empty($this->getSetting('country'))) {
      $summary[] = $this->t('Country: @country', ['@country' => $this->getSetting('country')]);
    }

    // Language.
    if (!empty($this->getSetting('language'))) {
      $summary[] = $this->t('Language: @language', ['@language' => $this->getSetting('language')]);
    }

    // Size.
    $summary[] = $this->t('Textfield size: @size', ['@size' => $this->getSetting('size')]);

    // Placeholder.
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = $this->t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'textfield',
      '#field_prefix' => '<div id="geocoder"></div>',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#size' => $this->getSetting('size'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#maxlength' => $this->getFieldSetting('max_length'),
      '#mapbox_options' => [
        'autocomplete' => $this->getSetting('autocomplete'),
        'types' => implode(',', $this->getSetting('types')),
        'limit' => $this->getSetting('limit'),
        'country' => $this->getSetting('country'),
        'language' => $this->getSetting('language'),
      ],
    ];
    return $element;
  }

}
