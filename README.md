## Mapbox Places

Module provides field widget that uses the mapbox-gl-geocoder
control to search for places using Mapbox Geocoding API.

### Requirements

Requires Mapbox access token:
https://docs.mapbox.com/help/getting-started/access-tokens/

Requires the Key module:
https://www.drupal.org/project/key

### Install/Usage

* Install and enable module like any other contributed module.
* Configure Mapbox Access token: **/admin/config/mapbox-places**
* Configure or create new textfield field on Entity.
* Goto "Manage form display" and select the textfield use wish to
use Mapbox places field widget with. Max places widget should be
available for selection.
* Configure any necessary settings for the field widget.
* Save form display settings.
* You should now be able to use the widget on entity add/edit forms.

Inspired by Algolia Places module:
https://www.drupal.org/project/algolia_places

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
