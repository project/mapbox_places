(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.mapbox_places = {
    attach: function (context, settings) {
      // Check and make sure access token is set.
      if (!drupalSettings.mapbox_places || !drupalSettings.mapbox_places.accessToken) {
        return;
      }

      // Set access token.
      mapboxgl.accessToken = drupalSettings.mapbox_places.accessToken;

      // Construct Mapbox Geocoder.
      const geocoder = new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        types: drupalSettings.mapbox_places.configOptions.types,
        autocomplete: drupalSettings.mapbox_places.configOptions.autocomplete,
        limit: drupalSettings.mapbox_places.configOptions.limit,
        country: drupalSettings.mapbox_places.configOptions.country,
        language: drupalSettings.mapbox_places.configOptions.language
      });
      geocoder.addTo('#geocoder');

      // Geocoder result event.
      geocoder.on('result', (e) => {
        if (e.result) {
          if ($(drupalSettings.mapbox_places.querySelector).length > 0) {
            $(drupalSettings.mapbox_places.querySelector).val(e.result.place_name);
          }
        }
      });

      // Geocoder clear event.
      geocoder.on('clear', () => {
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
